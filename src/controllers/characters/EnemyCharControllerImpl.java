package controllers.characters;

import controllers.commons.BasicController;
import models.commons.Point;
import models.commons.Point$;
import models.configs.CharacterConfig;
import utils.Utils;

/**
 *
 * Created by Alessandro on 12/03/2017.
 */
public class EnemyCharControllerImpl extends CharacterControllerImpl {

    private int offsetX;

    public EnemyCharControllerImpl(CharacterConfig config, Point<Integer> position) {
        super(config, position);

        super.setToRight(true);
        super.setMoving(true);
        this.offsetX = 1;
    }

    @Override
    public Point<Integer> move() {
        if (this.isAlive()){
            this.offsetX = this.isToRight() ? 1 : -1;
            super.setX(super.getX() + this.offsetX);
        }

        return Point$.MODULE$.apply(this.getX(), this.getY());
    }

    @Override
    public boolean computeCollision(BasicController<?> basicController) {
        if(basicController instanceof CharacterController){
            return this.contactWithACharacter((CharacterController) basicController);
        } else {
            return this.contactWithAnEnvironmentalObject(basicController);
        }
    }

    private boolean contactWithAnEnvironmentalObject(BasicController<?> envObj) {

        boolean ret = super.computeCollision(envObj);

        if (Utils.hitAhead(this, envObj)){
            this.setMoving(false);
            this.setToRight(false);
            this.offsetX = -1;

            ret = true;
        } else if(Utils.hitBack(this, envObj)) {

            this.setMoving(false);
            this.setToRight(true);
            this.offsetX = 1;

            ret = true;
        }

        return ret;
    }

    private boolean contactWithACharacter(CharacterController pers) {

        if (this.isAlive() && pers.isAlive()){
            if (Utils.hitBelow(this, pers)) {
                return true;
            } else if (Utils.hitAhead(this, pers)){
                if (!(pers instanceof MainCharController)){
                    this.setToRight(false);
                    this.offsetX = -1;
                }
                return true;

            } else if (Utils.hitBack(this, pers)) {
                if (!(pers instanceof MainCharController)) {
                    this.setToRight(true);
                    this.offsetX = 1;
                }
                return true;
            }
        }

        return false;
    }
}
