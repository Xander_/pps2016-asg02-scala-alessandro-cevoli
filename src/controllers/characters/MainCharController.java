package controllers.characters;

import controllers.commons.BasicController;
import models.configs.ObjectConfig;

/**
 *
 * Created by Alessandro on 12/03/2017.
 */
public interface MainCharController extends CharacterController {

    boolean collisionWithASpecialObject(BasicController<ObjectConfig> specialObject);
}
