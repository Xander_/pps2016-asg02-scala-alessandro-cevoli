package controllers.characters;

import controllers.commons.BasicController;
import controllers.commons.BasicControllerImpl;

import models.commons.BoundingBox$;
import models.commons.Dimensions$;
import models.commons.Point;
import models.configs.CharacterConfig;
import models.configs.GameEnvironmentConfig$;
import utils.Utils;

/**
 *
 * Created by Alessandro on 12/03/2017.
 */
public abstract class CharacterControllerImpl extends BasicControllerImpl<CharacterConfig>
        implements CharacterController {

    private static final int PROXIMITY_MARGIN = 10;

    private final CharacterConfig config;

    private boolean moving;
    private boolean toRight;
    private volatile boolean alive;
    private boolean jumping;
    private int jumpingExtent;
    private int floorOffsetY = GameEnvironmentConfig$.MODULE$.FLOOR_DEFAULT_OFFSET_Y();
    private int heightLimit = GameEnvironmentConfig$.MODULE$.HEIGHT_LIMIT();

    public CharacterControllerImpl(CharacterConfig config, Point<Integer> position){

        super(BoundingBox$.MODULE$.apply(Dimensions$.MODULE$.apply(config.width(), config.height()),
                position));

        this.config = config;
        this.moving = false;
        this.toRight = true;
        this.alive = true;
        this.jumping = false;
        this.jumpingExtent = 0;
    }

    @Override
    public CharacterConfig getConfig(){
        return this.config;
    }

    @Override
    public boolean isNearby(BasicController basicController) {
        return (this.getX() > basicController.getX() - PROXIMITY_MARGIN &&
                this.getX() < basicController.getX() + basicController.getWidth() + PROXIMITY_MARGIN) ||
                (this.getX() + this.getWidth() > basicController.getX() - PROXIMITY_MARGIN &&
                        this.getX() + this.getWidth() < basicController.getX() + basicController.getWidth() + PROXIMITY_MARGIN);
    }

    @Override
    public boolean isAlive() {
        return alive;
    }

    @Override
    public void setAlive(boolean isAlive) {
        this.alive = isAlive;
    }

    @Override
    public void setMoving(boolean isMoving) {
        this.moving = isMoving;
    }

    @Override
    public boolean isMoving() {
        return moving;
    }

    @Override
    public boolean isToRight() {
        return toRight;
    }

    @Override
    public void setToRight(boolean toRight) {
        this.toRight = toRight;
    }

    @Override
    public boolean isJumping() {
        return jumping;
    }

    @Override
    public void setJumping(boolean jumping) {
        this.jumping = jumping;
    }


    @Override
    public int getJumpingExtent() {
        return this.jumpingExtent;
    }

    @Override
    public void setJumpExtent(int newExtent) {
        this.jumpingExtent = newExtent;
    }

    @Override
    public int getFloorOffsetY() {
        return this.floorOffsetY;
    }

    @Override
    public void setFloorOffSetY(int offsetY) {
        this.floorOffsetY = offsetY;
    }

    @Override
    public int getHeightLimit() {
        return this.heightLimit;
    }

    @Override
    public void setHeightLimit(int maxHeight) {
        this.heightLimit = maxHeight;
    }

    @Override
    public boolean computeCollision(BasicController<?> envObj) {

        boolean ret = false;
        boolean hitBelow = Utils.hitBelow(this, envObj);

        if (hitBelow) {
            this.setFloorOffSetY(envObj.getY());
            ret = true;
        } else {
            this.setFloorOffSetY(GameEnvironmentConfig$.MODULE$.FLOOR_DEFAULT_OFFSET_Y());

            if (!this.isJumping()) {
                this.setY(this.getConfig().defaultOffsetY());
            }

            boolean hitAbove = Utils.hitAbove(this, envObj);

            if (hitAbove) {
                this.setHeightLimit(envObj.getY() + envObj.getHeight()); // the new sky goes below the object
            } else if (!this.isJumping()) {
                this.setHeightLimit(0); // initial sky
            }

            ret = true;
        }

        return ret;
    }
}
