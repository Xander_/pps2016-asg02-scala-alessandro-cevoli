package controllers.characters;

import controllers.commons.BasicController;
import models.commons.Point;
import models.configs.CharacterConfig;
import models.configs.GameEnvironmentConfig$;
import models.configs.ObjectConfig;
import utils.Utils;

import static models.commons.Point$.MODULE$;

/**
 *
 * Created by Alessandro on 12/03/2017.
 */
public class MainCharControllerImpl extends CharacterControllerImpl implements MainCharController {

    public MainCharControllerImpl(CharacterConfig config, Point<Integer> position){
        super(config, position);
    }

    @Override
    public Point<Integer> move(){
        // DO NOTHING
        return MODULE$.apply(this.getX(), this.getY());
    }

    @Override
    public boolean collisionWithASpecialObject(BasicController<ObjectConfig> specialObject) {

        return Utils.hitBack(this, specialObject) || Utils.hitAbove(this, specialObject)
                || Utils.hitAhead(this, specialObject) || Utils.hitBelow(this, specialObject);
    }

    @Override
    public boolean computeCollision(BasicController<?> basicController) {
        if(basicController instanceof CharacterController){
            return this.contactWithACharacter((CharacterController) basicController);
        } else {
            return this.contactWithAnEnvironmentalObject(basicController);
        }
    }

    private boolean contactWithAnEnvironmentalObject(BasicController<?> envObj) {

        boolean ret = super.computeCollision(envObj);

        if (Utils.hitAhead(this, envObj) && this.isToRight() ||
                Utils.hitBack(this, envObj) && !this.isToRight()) {

            GameEnvironmentConfig$.MODULE$.MOV_$eq(0);
            this.setMoving(false);
            ret = true;
        }

        return ret;
    }

    private boolean contactWithACharacter(CharacterController pers) {
        if (this.isAlive() && pers.isAlive()){
            if (Utils.hitBelow(this, pers)) {
                pers.setMoving(false);
                pers.setAlive(false);
                return true;

            } else if ((Utils.hitAhead(this, pers) || Utils.hitBack(this, pers))) {
                this.setMoving(false);
                this.setAlive(false);
                return true;
            }
        }

        return false;
    }
}
