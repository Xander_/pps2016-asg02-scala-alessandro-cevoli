package controllers.objects;

import controllers.commons.BasicControllerImpl;

import models.commons.*;
import models.configs.GameEnvironmentConfig$;
import models.configs.ObjectConfig;

/**
 *
 * Created by Alessandro on 12/03/2017.
 */
public class ObjectControllerImpl extends BasicControllerImpl<ObjectConfig>
        implements ObjectController {

    private ObjectConfig config;
    private volatile boolean consumed;
    private volatile boolean isConsumable = false;

    public ObjectControllerImpl(ObjectConfig config, Point<Integer> position, boolean isConsumable) {
        super(BoundingBox$.MODULE$.apply(Dimensions$.MODULE$.apply(config.width(), config.height()), position));
        this.config = config;
        this.isConsumable = isConsumable;
    }

    @Override
    public boolean isConsumed() {
        return isConsumable ? consumed : false;
    }

    @Override
    public void consume() {
        consumed = isConsumable ? true : false;
    }

    @Override
    public Point<Integer> move() {
        if (GameEnvironmentConfig$.MODULE$.X_POS() >= 0) {
            this.setX(this.getX() - GameEnvironmentConfig$.MODULE$.MOV());
        }

        return Point$.MODULE$.apply(this.getX(), this.getY());
    }

    @Override
    public ObjectConfig getConfig(){
        return this.config;
    }

    @Override
    public boolean equals(ObjectController environmentalObject) {
        return getConfig().name().equals(environmentalObject.getConfig().name()) &&
                getX() == environmentalObject.getX() && getY() == environmentalObject.getY();
    }
}
