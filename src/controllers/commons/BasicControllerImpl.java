package controllers.commons;

import models.commons.BoundingBox;
import models.commons.Dimensions$;
import models.commons.Point$;

/**
 *
 * Created by Xander_C on 11/03/2017.
 */
public abstract class BasicControllerImpl<C> implements BasicController<C> {

    private BoundingBox<Integer> boundingBox;

    public BasicControllerImpl(BoundingBox<Integer> boundingBox) {
        this.boundingBox = boundingBox;
    }

    public int getWidth() {
        return boundingBox.dimensions().width();
    }

    public int getHeight() {
        return boundingBox.dimensions().height();
    }

    public int getX() {
        return boundingBox.position().X();
    }

    public int getY() {
        return boundingBox.position().Y();
    }

    public void setWidth(int width) {
        this.boundingBox.$colon$eq(Dimensions$.MODULE$.apply(width, this.getHeight()));
    }

    public void setHeight(int height) {
        this.boundingBox.$colon$eq(Dimensions$.MODULE$.apply(this.getWidth(), height));
    }

    public void setX(int x) {
        this.boundingBox.$colon$eq(Point$.MODULE$.apply(x, this.getY()));
    }

    public void setY(int y) {
        this.boundingBox.$colon$eq(Point$.MODULE$.apply(this.getX(), y));
    }

}
