package controllers.core

import core.characters.GameCharacter
import core.objects.{GameObject, SpecialObject}
import models.configs.GameEnvironmentConfig._
import models.configs.{AudioConfig, CharacterConfig, ObjectConfig}
import models.core.GameModel
import utils.Utils

/**
  *
  * Created by Xander_C on 13/03/2017.
  */
trait GameController {
    def computeCollisions(): Unit
    
    def updateWorldElements(): Unit
    
    def model: GameModel
}

object GameController {
    private[this] case class GameControllerImpl(model: GameModel) extends GameController{
        
        override def computeCollisions(): Unit = {
            // Collisions with Environment
            model.charactersList.toStream
                .foreach((c: GameCharacter) => model.environmentObjectsList.toStream
                    .filter((go: GameObject) =>
//                        (go.getConfig == ObjectConfig.HIDDEN_BLOCK && go.getController.isConsumed) ||
                            (!go.getConfig.canBeTotallyConsumed || !go.getController.isConsumed) &&
                                c.getController.isNearby(go.getController))
                    .foreach((go: GameObject) => c.getController.computeCollision(go.getController)))
            
            // Collision between Characters
            model.charactersList.toStream
                .foreach((cA: GameCharacter) => model.charactersList.toStream
                    .filter((cB: GameCharacter) => !(cA == cB) && cA.getController.isNearby(cB.getController))
                    .foreach((cB: GameCharacter) => cA.getController.computeCollision(cB.getController)))
            
            // Consume Blocks
            model.environmentObjectsList.toStream
                .filter((go: GameObject) => go.isConsumable && !go.getController.isConsumed &&
                    Utils.hitAbove(model.mainCharacter.getController, go.getController))
                .foreach((go: GameObject) => {
                    go.getController.consume()
                    if (go.getConfig.canBeTotallyConsumed)
                        model.mainCharacter.getController.setHeightLimit(CharacterConfig.JUMPING_LIMIT)
                    if(go.getConfig == ObjectConfig.HIDDEN_BLOCK)
                        AudioManager.playSound(AudioConfig.AUDIO_COIN.soundSource)
                })
            
            // Consuming SpecialObjects
            model.specialObjectsList.toStream
                .filter((so: SpecialObject) => !so.getController.isConsumed &&
                    model.mainCharacter.getController.isNearby(so.getController) &&
                    model.mainCharacter.getController.collisionWithASpecialObject(so.getController))
                .foreach((so: SpecialObject) => {so.getController.consume(); AudioManager.playSound(AudioConfig.AUDIO_COIN.soundSource)})
        }
    
        override def updateWorldElements(): Unit = {
            this.updateBackgroundOnMovement()
            this.moveCPUControlledElements()
        }
    
        private def updateBackgroundOnMovement(): Unit = {
            if (X_POS >= 0 && X_POS <= 4600) {
                X_POS += MOV
                
                // Moving the screen to give the impression that MainCharacterImpl is walking
                BACKGROUND_A.position.X = BACKGROUND_A.position.X - MOV
                BACKGROUND_B.position.X = BACKGROUND_B.position.X - MOV
            }
            
            // Flipping between background1 and background2
            if (BACKGROUND_A.position.X == - BACKGROUND_FLIPPING_OFFSET)
                BACKGROUND_A.position.X = BACKGROUND_FLIPPING_OFFSET
            else if (BACKGROUND_B.position.X == - BACKGROUND_FLIPPING_OFFSET)
                BACKGROUND_B.position.X = BACKGROUND_FLIPPING_OFFSET
            else if (BACKGROUND_A.position.X == BACKGROUND_FLIPPING_OFFSET)
                BACKGROUND_A.position.X = - BACKGROUND_FLIPPING_OFFSET
            else if (BACKGROUND_B.position.X == BACKGROUND_FLIPPING_OFFSET)
                BACKGROUND_B.position.X = - BACKGROUND_FLIPPING_OFFSET
        }
    
        private def moveCPUControlledElements(): Unit = {
            if (X_POS >= ENVIRONMENT_MIN_X  && X_POS <= ENVIRONMENT_MAX_X) {
                model.environmentObjectsList.toStream
                    .filter((go: GameObject) => !go.getConfig.canBeTotallyConsumed || !go.getController.isConsumed )
                    .foreach((go: GameObject) => go.getController.move)
                model.specialObjectsList.toStream
                    .filter((so: SpecialObject) => !so.getController.isConsumed)
                    .foreach((so: SpecialObject) => so.getController.move)
                model.charactersList.toStream
                    .filter((c: GameCharacter) => c.getController.isAlive)
                    .foreach((c: GameCharacter) => c.getController.move)
            }
        }
    }
    
    def apply(model: GameModel): GameController = GameControllerImpl(model)
}