package controllers.core;

import core.characters.MainCharacter;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class KeyboardManager extends Observable implements KeyListener {

    private MainCharacter main;

    private boolean isKeyPressed;
    private boolean isKeyReleased;
    private boolean isKeyTyped;
    private List<Observer> observerList;

    public KeyboardManager(MainCharacter main){

        this.main = main;
    }

    public KeyboardManager(List<Observer> observerList){
        this.observerList = observerList;
    }

    @Override
    public void keyPressed(KeyEvent e) {

        this.isKeyPressed = true;

        this.observerList.forEach(o -> o.update(this, e));

        this.isKeyPressed = false;
    }

    @Override
    public void keyReleased(KeyEvent e) {
        this.isKeyReleased = true;

        this.observerList.forEach(o -> o.update(this, e));

        this.isKeyReleased = false;
    }

    @Override
    public void keyTyped(KeyEvent e) {
        this.isKeyTyped = true;

        this.observerList.forEach(o -> o.update(this, e));

        this.isKeyTyped = false;
    }

    public boolean isKeyPressed() {
        return isKeyPressed;
    }

    public boolean isKeyReleased() {
        return isKeyReleased;
    }

    public boolean isKeyTyped() {
        return isKeyTyped;
    }
}
