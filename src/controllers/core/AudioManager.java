package controllers.core;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class AudioManager {
    private Clip clip;

    public AudioManager(String son) {

        try {
            AudioInputStream audio = AudioSystem.getAudioInputStream(this.getClass().getResource(son));

            clip = AudioSystem.getClip();
            clip.open(audio);
        } catch (Exception e) {
            // TODO: log error
        }
    }

    public Clip getClip() {
        return clip;
    }

    public void play() {
        clip.start();
    }

    public void stop() {
        clip.stop();
    }

    public static void playSound(String son) {
        AudioManager s = new AudioManager(son);
        s.play();
        //s.stop();
    }
}
