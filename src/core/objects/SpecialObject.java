package core.objects;

import controllers.objects.ObjectController;
import views.commons.AnimatedElementViewer;

/**
 *
 * Created by Xander_C on 13/03/2017.
 */
public interface SpecialObject extends GameObject {

    @Override
    AnimatedElementViewer<ObjectController> getViewer();

    @Override
    default boolean isConsumable() {
        return true;
    }
}
