package core.objects;

import controllers.objects.ObjectController;
import core.commons.GameElement;
import models.configs.ObjectConfig;
import views.commons.SimpleElementViewer;

public class GameObjectImpl implements GameObject {

    private SimpleElementViewer<ObjectController> viewer;
    private boolean isConsumable;

    public GameObjectImpl(
            final SimpleElementViewer<ObjectController> viewer, boolean isConsumable) {

        this.viewer = viewer;
        this.isConsumable = isConsumable;
    }

    @Override
    public boolean isConsumable() {
        return isConsumable;
    }

    @Override
    public ObjectController getController() {
        return getViewer().getController();
    }

    @Override
    public ObjectConfig getConfig() {
        return getViewer().getController().getConfig();
    }

    @Override
    public SimpleElementViewer<ObjectController> getViewer() {
        return viewer;
    }

    @Override
    public boolean equals(GameElement<ObjectConfig, ObjectController, SimpleElementViewer<ObjectController>> gameElement) {
        return this.getConfig().name().equals(gameElement.getConfig().name()) &&
                this.getController().getX() == gameElement.getController().getX() &&
                this.getController().getY() == gameElement.getController().getY();
    }
}
