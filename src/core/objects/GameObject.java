package core.objects;

import controllers.objects.ObjectController;
import core.commons.GameElement;
import models.configs.ObjectConfig;
import views.commons.SimpleElementViewer;

/**
 *
 * Created by Xander_C on 11/03/2017.
 */
public interface GameObject
        extends GameElement<ObjectConfig, ObjectController, SimpleElementViewer<ObjectController>>{

    // Entry Point to cast Game Element Generics
    boolean isConsumable();
}
