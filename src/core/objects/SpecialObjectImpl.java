package core.objects;

import controllers.objects.ObjectController;
import views.commons.AnimatedElementViewer;

public class SpecialObjectImpl extends GameObjectImpl implements SpecialObject, Runnable {

    private static final int PAUSE = 10;

    public SpecialObjectImpl(AnimatedElementViewer<ObjectController> viewer) {
        super(viewer,true);
    }

    @Override
    public AnimatedElementViewer<ObjectController> getViewer(){
        return (AnimatedElementViewer<ObjectController>) super.getViewer();
    }

    @Override
    public void run() {
        while (!this.getController().isConsumed()) {
            this.getViewer().setAnimationStepCounter(this.getViewer().getAnimationStepCounter()+1);
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
