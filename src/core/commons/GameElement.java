package core.commons;

public interface GameElement<X, Y, Z> {

    X getConfig();

    Y getController();

    Z getViewer();

    boolean equals(GameElement<X, Y, Z> gameElement);

}
