package core.characters;

import controllers.characters.CharacterController;
import core.commons.GameElement;
import models.configs.CharacterConfig;
import views.commons.AnimatedElementViewer;

public interface GameCharacter
        extends GameElement<CharacterConfig, CharacterController, AnimatedElementViewer<CharacterController>> {

    // Entry Point to cast GameElement Generics

    boolean canJump();

}
