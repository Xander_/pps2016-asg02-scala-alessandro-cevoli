package core.characters;

import controllers.characters.CharacterController;
import controllers.characters.MainCharController;
import controllers.core.AudioManager;
import controllers.core.KeyboardManager;
import models.configs.AudioConfig$;
import models.configs.GameEnvironmentConfig$;
import views.commons.AnimatedElementViewer;

import java.awt.event.KeyEvent;
import java.util.Observable;
import java.util.Observer;

public class MainCharacterImpl extends BasicCharacter implements MainCharacter, Observer {

    public MainCharacterImpl(AnimatedElementViewer<CharacterController> viewer) {
        super(viewer);
    }

    @Override
    public MainCharController getController() {
        return (MainCharController) super.getController();
    }

    @Override
    public void update(Observable o, Object arg) {

        if (o instanceof KeyboardManager && arg instanceof KeyEvent){
            KeyEvent e = (KeyEvent) arg;

            KeyboardManager km = (KeyboardManager) o;

            if(km.isKeyPressed()){
                this.updateOnKeyPressed(e);
            } else if(km.isKeyReleased()){
                this.updateOnKeyReleased(e);
            } else if(km.isKeyTyped()){
                this.updateOnKeyTyped(e);
            }
        }
    }

    private void updateOnKeyPressed(KeyEvent e){

        if (this.getController().isAlive()) {

            if (e.getKeyCode() == KeyEvent.VK_RIGHT || e.getKeyCode() == KeyEvent.VK_D) {

                // per non fare muovere il castello e start
                if (GameEnvironmentConfig$.MODULE$.X_POS() == GameEnvironmentConfig$.MODULE$.ENVIRONMENT_MIN_X()-1) {

                    GameEnvironmentConfig$.MODULE$.X_POS_$eq(GameEnvironmentConfig$.MODULE$.ENVIRONMENT_MIN_X());

                    GameEnvironmentConfig$.MODULE$.BACKGROUND_A().position()
                            .X_$eq(GameEnvironmentConfig$.MODULE$.BACKGROUND_A_DEFAULT_X_POS());
                    GameEnvironmentConfig$.MODULE$.BACKGROUND_B().position()
                            .X_$eq(GameEnvironmentConfig$.MODULE$.BACKGROUND_B_DEFAULT_X_POS());
                }

                this.getViewer().setAnimationStepCounter(this.getViewer().getAnimationStepCounter()+1);

                this.getController().setMoving(true);
                this.getController().setToRight(true);

                GameEnvironmentConfig$.MODULE$.MOV_$eq(1); // si muove verso sinistra

            } else if (e.getKeyCode() == KeyEvent.VK_LEFT || e.getKeyCode() == KeyEvent.VK_A) {

                if (GameEnvironmentConfig$.MODULE$.X_POS() == GameEnvironmentConfig$.MODULE$.ENVIRONMENT_MAX_X()+1) {

                    GameEnvironmentConfig$.MODULE$.X_POS_$eq(GameEnvironmentConfig$.MODULE$.ENVIRONMENT_MAX_X());

                    GameEnvironmentConfig$.MODULE$.BACKGROUND_A().position()
                            .X_$eq(GameEnvironmentConfig$.MODULE$.BACKGROUND_A_DEFAULT_X_POS());
                    GameEnvironmentConfig$.MODULE$.BACKGROUND_B().position()
                            .X_$eq(GameEnvironmentConfig$.MODULE$.BACKGROUND_B_DEFAULT_X_POS());
                }

                this.getViewer().setAnimationStepCounter(this.getViewer().getAnimationStepCounter()+1);

                this.getController().setMoving(true);
                this.getController().setToRight(false);

                GameEnvironmentConfig$.MODULE$.MOV_$eq(-1); // si muove verso destra
            }

            // salto
            if (e.getKeyCode() == KeyEvent.VK_UP ||
                    e.getKeyCode() == KeyEvent.VK_SPACE ||
                    e.getKeyCode() == KeyEvent.VK_W) {
                this.getController().setJumping(true);
                AudioManager.playSound(AudioConfig$.MODULE$.AUDIO_JUMP().soundSource());
            }

        }

    }

    private void updateOnKeyReleased(KeyEvent e){
        this.getController().setMoving(false);
        GameEnvironmentConfig$.MODULE$.MOV_$eq(0);
    }

    private void updateOnKeyTyped(KeyEvent e){
        // NOPE
    }
}