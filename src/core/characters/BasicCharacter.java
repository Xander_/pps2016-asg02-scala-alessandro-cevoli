package core.characters;

import controllers.characters.CharacterController;
import core.commons.GameElement;
import models.configs.CharacterConfig;
import views.commons.AnimatedElementViewer;

public abstract class BasicCharacter implements GameCharacter {

    private AnimatedElementViewer<CharacterController> view;

    protected BasicCharacter(final AnimatedElementViewer<CharacterController> view) {
        this.view = view;

    }

    @Override
    public CharacterConfig getConfig() {
        return this.getViewer().getController().getConfig();
    }

    @Override
    public CharacterController getController() {
        return this.getViewer().getController();
    }

    @Override
    public AnimatedElementViewer<CharacterController> getViewer() {
        return this.view;
    }

    @Override
    public boolean equals(GameElement<CharacterConfig, CharacterController,
            AnimatedElementViewer<CharacterController>> gameCharacter) {

        return this.getConfig().name().equals(gameCharacter.getConfig().name()) &&
                this.getController().getX() == gameCharacter.getController().getX() &&
                this.getController().getY() == gameCharacter.getController().getY();
    }
}