package core.characters;

import controllers.characters.CharacterController;
import models.configs.GameEnvironmentConfig$;
import views.commons.AnimatedElementViewer;

import static models.configs.CharacterConfig$.MODULE$;

/**
 *
 * Created by Xander_C on 11/03/2017.
 */
public class EnemyCharacterImpl extends BasicCharacter implements Runnable{

    private static int JUMP_FREQUENCY = 33;
    private final boolean canJump;

    private Thread instance;

    public EnemyCharacterImpl(AnimatedElementViewer<CharacterController> viewer, boolean canJump){
        super(viewer);
        this.canJump = canJump;

        instance = new Thread(this);
        instance.start();
    }

    @Override
    public boolean canJump() {
        return canJump;
    }

    @Override
    public void run() {
        while (this.getController().isAlive()) {

            if(this.getController().getX() <= GameEnvironmentConfig$.MODULE$.ENVIRONMENT_MIN_X()){
                //this.getController().setToRight(true);
                this.getController().setX(GameEnvironmentConfig$.MODULE$.ENVIRONMENT_MAX_X());
            } else if(this.getController().getX() >= GameEnvironmentConfig$.MODULE$.ENVIRONMENT_MAX_X()){
                //this.getController().setToRight(false);
                this.getController().setX(GameEnvironmentConfig$.MODULE$.ENVIRONMENT_MIN_X());
            }

            if (this.canJump() && !this.getController().isJumping() &&
                    ((int)(Math.random()*Math.random()*1000))%JUMP_FREQUENCY == 0){
                this.getController().setJumping(true);
            }

            this.getController().move();
            this.getViewer().setAnimationStepCounter(this.getViewer().getAnimationStepCounter()+1);

            try {
                Thread.sleep(MODULE$.PAUSE());
            } catch (InterruptedException e) {
                // DO NOTHING
                e.printStackTrace();
            }
        }
    }
}
