package utils

import java.awt.Image

import controllers.characters.CharacterController
import models.configs.CharacterConfig._
import models.configs.Res._
import views.commons.AnimatedElementViewer

/**
  * Created by Alessandro on 09/04/2017.
  */
object AnimationLogics {
    
    def jumpAnimation(v: AnimatedElementViewer[CharacterController]): Image = v.getController match {
        case c : CharacterController =>
            
            c.setJumpExtent(c.getJumpingExtent + 1)
            
            if (c.getJumpingExtent < JUMPING_LIMIT)
                if (c.getY > c.getHeightLimit)
                    c.setY(c.getY - 4)
                else c.setJumpExtent(JUMPING_LIMIT)
            else if (c.getY + c.getHeight < c.getFloorOffsetY)
                c.setY(c.getY + 1)
            else {
                c.setJumping(false)
                c.setJumpExtent(0)
                if (c.isToRight) buildIdleImage(c.getConfig.name, DIR_DX)
                else buildIdleImage(c.getConfig.name, DIR_SX)
            }
            
            if (c.isToRight) buildJumpImage(c.getConfig.name, DIR_DX)
            else buildJumpImage(c.getConfig.name, DIR_SX)
        case _ => null
    }
    
    def walkAnimation(v: AnimatedElementViewer[CharacterController]): Image = v.getController match {
        case c : CharacterController =>
            if (!c.isMoving || v.getAnimationStepCounter % c.getConfig.frequency == 0)
                buildIdleImage(c.getConfig.name, if (c.isToRight) DIR_DX else DIR_SX)
            else buildWalkImage(c.getConfig.name, if (c.isToRight) DIR_DX else DIR_SX)
        case _ => null
        
    }
    
    def deathAnimation(v: AnimatedElementViewer[CharacterController]): Image = v.getController match {
        case c : CharacterController =>
            if (c.isToRight) buildDeathImage(c.getConfig.name, DIR_DX)
            else buildDeathImage(c.getConfig.name, DIR_SX)
        case _ => null
    }
}