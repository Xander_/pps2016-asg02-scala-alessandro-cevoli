package utils;

import controllers.characters.CharacterController;
import controllers.commons.BasicController;

import javax.swing.*;
import java.awt.*;
import java.net.URL;

/**
 * @author Roberto Casadei
 */

public class Utils {

    private static final int HIT_BOX_OFFSET = 4;

    public static URL getResource(String path){
        return Utils.class.getClass().getResource(path);
    }

    public static Image getImage(String path){
        return new ImageIcon(getResource(path)).getImage();
    }

    public static boolean hitAhead(BasicController hitter, BasicController hitted) {

        boolean hitCondition = !(hitter.getX() + hitter.getWidth() < hitted.getX() ||
                hitter.getX() + hitter.getWidth() > hitted.getX() + HIT_BOX_OFFSET ||
                hitter.getY() + hitter.getHeight() <= hitted.getY() ||
                hitter.getY() >= hitted.getY() + hitted.getHeight());

        if (hitted instanceof CharacterController){
            hitCondition = ((CharacterController) hitted).isToRight() && hitCondition;
        }

        return hitCondition;

    }

    public static boolean hitBack(BasicController hitter, BasicController hitted) {

        return !(hitter.getX() > hitted.getX() + hitted.getWidth() ||
                hitter.getX() + hitter.getWidth() < hitted.getX() + hitted.getWidth() - HIT_BOX_OFFSET ||
                hitter.getY() + hitter.getHeight() <= hitted.getY() ||
                hitter.getY() >= hitted.getY() + hitted.getHeight());
    }

    public static boolean hitBelow(BasicController hitter, BasicController hitted) {

        int offset = hitted instanceof CharacterController ? 0 : HIT_BOX_OFFSET;

        return !(hitter.getX() + hitter.getWidth() < hitted.getX() + offset ||
                hitter.getX() > hitted.getX() + hitted.getWidth() - offset ||
                hitter.getY() + hitter.getHeight() < hitted.getY() ||
                hitter.getY() + hitter.getHeight() > hitted.getY() + offset);
    }

    public static boolean hitAbove(BasicController hitter, BasicController hitted) {

        return !(hitter.getX() + hitter.getWidth() < hitted.getX() + HIT_BOX_OFFSET ||
                hitter.getX() > hitted.getX() + hitted.getWidth() - HIT_BOX_OFFSET ||
                hitter.getY() < hitted.getY() + hitted.getHeight() ||
                hitter.getY() > hitted.getY() + hitted.getHeight() + HIT_BOX_OFFSET);
    }
}
