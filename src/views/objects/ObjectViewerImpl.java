package views.objects;

import controllers.objects.ObjectController;
import views.commons.SimpleElementViewer;

/**
 *
 * Created by Alessandro on 12/03/2017.
 */
public class ObjectViewerImpl implements SimpleElementViewer<ObjectController> {

    private final ObjectController controller;

    public ObjectViewerImpl(ObjectController controller){

        this.controller = controller;
    }

    @Override
    public ObjectController getController() {
        return this.controller;
    }
}
