package views.objects;

import controllers.objects.ObjectController;
import views.commons.AnimatedElementViewer;

import java.awt.*;

/**
 *
 * Created by Xander_C on 13/03/2017.
 */
public class SpecialObjectViewerImpl extends ObjectViewerImpl implements AnimatedElementViewer<ObjectController> {

    private int flipAnimationCounter;

    public SpecialObjectViewerImpl(ObjectController controller) {
        super(controller);
    }

    @Override
    public Image animation(){
        return this.getController().getConfig().apply(this);
    }

    @Override
    public int getAnimationStepCounter() {
        return this.flipAnimationCounter;
    }

    @Override
    public void setAnimationStepCounter(int newCounter) {
        this.flipAnimationCounter = newCounter;
        this.flipAnimationCounter %= this.getController().getConfig().frequency();
    }
}
