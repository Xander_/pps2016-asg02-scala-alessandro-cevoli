package views.core

import java.awt.Graphics

import controllers.core.GameController
import core.characters.{GameCharacter, MainCharacter}
import core.objects.{GameObject, SpecialObject}

/**
  * Created by Alessandro on 09/04/2017.
  */
object GamePlatformScaLaScripts {
    
    def repaintWorldObjects(controller: GameController, graphics: Graphics):Unit = {
        controller.model.environmentObjectsList.toStream
            .filter((go: GameObject) => !go.getConfig.canBeTotallyConsumed || !go.getController.isConsumed)
            .foreach((go: GameObject) =>
                graphics.drawImage(go.getConfig.apply(go.getViewer), go.getController.getX, go.getController.getY, null))
    
        controller.model.specialObjectsList.toStream
            .filter((so: SpecialObject) => !so.getController.isConsumed)
            .foreach((so: SpecialObject) =>
                graphics.drawImage(so.getViewer.animation, so.getController.getX, so.getController.getY, null))
        
        controller.model.charactersList.toStream
            .filter({
                case _: MainCharacter => true
                case gc: GameCharacter => gc.getController.isAlive
            })
            .foreach((gc: GameCharacter) =>
                graphics.drawImage(gc.getViewer.animation, gc.getController.getX,
                gc.getController.getY + gc.getConfig.deathOffsetY(gc.getController), null))
    }
}
