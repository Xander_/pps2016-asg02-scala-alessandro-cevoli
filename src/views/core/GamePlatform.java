package views.core;

import controllers.core.GameController;
import controllers.core.KeyboardManager;

import javax.swing.*;
import java.awt.*;
import java.util.*;

import static models.configs.GameEnvironmentConfig$.MODULE$;

@SuppressWarnings("serial")
public class GamePlatform extends JPanel {

    private GameController controller;

    public GamePlatform(GameController controller){
        this.controller = controller;

        this.setFocusable(true);

        this.requestFocusInWindow();

        java.util.List<Observer> ol = new ArrayList<>();

        ol.add((Observer) this.controller.model().mainCharacter());

        this.addKeyListener(new KeyboardManager(ol));

        this.controller.model().populateWorld();
    }

    public void paintComponent(Graphics g) {

        super.paintComponent(g);

        this.controller.computeCollisions();

        this.controller.updateWorldElements();

        this.paintUpdatedWorldElements(g);

    }

    private void paintUpdatedWorldElements(Graphics graphics){

        graphics.drawImage(MODULE$.BACKGROUND_A().image(),
                MODULE$.BACKGROUND_A().position().X(),
                MODULE$.BACKGROUND_A().position().Y(), null);

        graphics.drawImage(MODULE$.BACKGROUND_B().image(),
                MODULE$.BACKGROUND_B().position().X(),
                MODULE$.BACKGROUND_B().position().Y(), null);

        graphics.drawImage(MODULE$.STARTING_CASTLE().image(),
                MODULE$.STARTING_CASTLE().position().X() - MODULE$.X_POS(),
                MODULE$.STARTING_CASTLE().position().Y() , null);

        graphics.drawImage(MODULE$.STARTING_SIGN().image(),
                MODULE$.STARTING_SIGN().position().X() - MODULE$.X_POS(),
                MODULE$.STARTING_SIGN().position().Y(), null);

        graphics.drawImage(MODULE$.ENDING_FLAGPOLE().image(),
                MODULE$.ENDING_FLAGPOLE().position().X() - MODULE$.X_POS(),
                MODULE$.ENDING_FLAGPOLE().position().Y(), null);

        graphics.drawImage(MODULE$.ENDING_CASTLE().image(),
                MODULE$.ENDING_CASTLE().position().X() - MODULE$.X_POS(),
                MODULE$.ENDING_CASTLE().position().Y(), null);

        GamePlatformScaLaScripts$.MODULE$.repaintWorldObjects(controller, graphics);

    }
}
