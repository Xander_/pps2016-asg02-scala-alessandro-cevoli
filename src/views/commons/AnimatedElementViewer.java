package views.commons;

import controllers.commons.BasicController;

import java.awt.*;

/**
 *
 * Created by Xander_C on 13/03/2017.
 */
public interface AnimatedElementViewer<C extends BasicController> extends SimpleElementViewer<C> {

    Image animation();

    int getAnimationStepCounter();

    void setAnimationStepCounter(int newCounter);
}
