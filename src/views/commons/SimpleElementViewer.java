package views.commons;

import controllers.commons.BasicController;

/**
 *
 * Created by Alessandro on 12/03/2017.
 */
public interface SimpleElementViewer<C extends BasicController>{

    C getController();

}
