package models.factories

import controllers.characters.{EnemyCharControllerImpl, MainCharControllerImpl}
import controllers.objects.{ObjectControllerImpl}
import core.characters._
import core.objects._
import models.commons.Point
import models.configs.{CharacterConfig, ObjectConfig}
import views.characters.CharacterViewerImpl
import views.objects.{ObjectViewerImpl, SpecialObjectViewerImpl}

/**
  * Created by Alessandro on 09/04/2017.
  */
trait GameElementsFactory {
    
    def createPlayerCharacter(config: CharacterConfig, position: Point[Integer]):MainCharacter
    
    def createEnemyCharacter(config: CharacterConfig, position: Point[Integer], canJump:Boolean): GameCharacter
    
    def createEnvironmentObject(config: ObjectConfig, position: Point[Integer], isConsumable: Boolean): GameObject
    
    def createSpecialObject(config: ObjectConfig, position: Point[Integer]): SpecialObject
}

object GameElementsFactory extends GameElementsFactory{
    
        override def createPlayerCharacter(config: CharacterConfig, position: Point[Integer]):MainCharacter =
            new MainCharacterImpl(new CharacterViewerImpl(new MainCharControllerImpl(config, position)))
    
        override def createEnemyCharacter(config: CharacterConfig, position: Point[Integer], canJump:Boolean): GameCharacter =
            new EnemyCharacterImpl(new CharacterViewerImpl(new EnemyCharControllerImpl(config, position)), canJump)
    
        override def createEnvironmentObject(config: ObjectConfig, position: Point[Integer], isConsumable: Boolean): GameObject =
            new GameObjectImpl(new ObjectViewerImpl(new ObjectControllerImpl(config, position, isConsumable)), isConsumable)
    
        override def createSpecialObject(config: ObjectConfig, position: Point[Integer]): SpecialObject =
            new SpecialObjectImpl(new SpecialObjectViewerImpl(new ObjectControllerImpl(config, position, true)))

}
