package models.commons

/**
  * Created by Alessandro on 08/04/2017.
  */
trait BoundingBox [T]{
    def position: Point[T]
    
    def := (newPosition: Point[T]): Unit
    
    def dimensions: Dimensions[T]
    
    def := (newDimensions: Dimensions[T]): Unit
    
    override def toString: String = this.dimensions.toString + "  in  " + this.position.toString
    
    override def equals(obj: scala.Any): Boolean = obj match {
        case that: BoundingBox[T] => this.dimensions == that.dimensions && this.position == that.position
        case _ => false
    }
}

object BoundingBox{
    case class BoundingBoxImpl [T] (var dimensions: Dimensions[T],
                                    var position:Point[T]) extends BoundingBox[T] {
        
        override def := (newPosition: Point[T]): Unit = this.position = newPosition
    
        override def := (newDimensions: Dimensions[T]): Unit = this.dimensions = newDimensions
    }
    
    def apply(dimensions: Dimensions[Integer], position:Point[Integer]): BoundingBox[Integer] = new BoundingBoxImpl[Integer](dimensions, position)
}

object TestBox extends App{
    
    val box:BoundingBox[Integer] = BoundingBox(Dimensions(4, 4), Point(0,0))
    box := Dimensions(10, 10)
}