package models.commons

/**
  * Created by Alessandro on 08/04/2017.
  */
trait Dimensions[T]{
    def width: T
    def height: T
    def width_=(width: T): Unit
    def height_=(height: T): Unit
    
    override def toString: String = "Dimensions[" + this.width + ", " + this.height + "]"
    
    override def equals(obj: scala.Any): Boolean = obj match{
        case that: Dimensions[T] => this.width == that.width && this.height == that.height
    }
}

object Dimensions {
    case class Dimensions2D[T] (var width:T, var height:T) extends Dimensions[T]
    
    def apply(width: Integer, height: Integer): Dimensions[Integer] = new Dimensions2D[Integer](width, height)
}

trait Point[T]{
    def X: T
    def Y: T
    def X_=(x:T): Unit
    def Y_=(y:T): Unit
    
    override def toString: String = "Point[" +  this.X + ", " + this.Y + "]"
    
    override def equals(obj: scala.Any): Boolean = obj match {
        case that : Point[T] => this.X == that.X && this.Y == that.Y
        case _ => false
    }
}

object Point {
    
    case class Point2D[T](var X: T, var Y: T) extends Point[T]
    
    def apply(x: Integer, y: Integer): Point[Integer] = new Point2D[Integer](x, y)
}

object TestGeometry extends App{
    // TO DO
}
