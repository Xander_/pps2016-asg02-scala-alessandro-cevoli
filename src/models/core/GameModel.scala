package models.core

import core.characters.{GameCharacter, MainCharacter}
import core.objects.{GameObject, SpecialObject}
import models.commons.Point
import models.configs.CharacterConfig._
import models.configs.ObjectConfig._
import models.factories.GameElementsFactory._

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

/**
  *
  * Created by Xander_C on 13/03/2017.
  */
trait GameModel {
    def populateWorld(): Unit
    
    def environmentObjectsList: List[GameObject]
    
    def charactersList: List[GameCharacter]
    
    def specialObjectsList: List[SpecialObject]
    
    def mainCharacter: MainCharacter
}

class GameModelImpl() extends GameModel {

    private val characters:mutable.Buffer[GameCharacter] = new ListBuffer[GameCharacter]
    private val objects:mutable.Buffer[GameObject] = new ListBuffer[GameObject]
    
    private val specialObjects:mutable.Buffer[SpecialObject] = new ListBuffer[SpecialObject]
    private val player:MainCharacter = createPlayerCharacter(MARIO, Point(300, 245))
    
    override def populateWorld(): Unit = {
    
        this.objects.append(
            createEnvironmentObject(PIPE, Point(600, 230), PIPE.canBeTotallyConsumed),
            createEnvironmentObject(PIPE, Point(1000, 230), PIPE.canBeTotallyConsumed),
            createEnvironmentObject(PIPE, Point(1600, 230), PIPE.canBeTotallyConsumed),
            createEnvironmentObject(PIPE, Point(1900, 230), PIPE.canBeTotallyConsumed),
            createEnvironmentObject(PIPE, Point(2500, 230), PIPE.canBeTotallyConsumed),
            createEnvironmentObject(PIPE, Point(3000, 230), PIPE.canBeTotallyConsumed),
            createEnvironmentObject(PIPE, Point(3800, 230), PIPE.canBeTotallyConsumed),
            createEnvironmentObject(PIPE, Point(4500, 230), PIPE.canBeTotallyConsumed),
            createEnvironmentObject(BLOCK, Point(400, 180), PIPE.canBeTotallyConsumed),
            createEnvironmentObject(BLOCK, Point(1200, 180), isConsumable = true),
            createEnvironmentObject(BLOCK, Point(1270, 170), isConsumable = false),
            createEnvironmentObject(BLOCK, Point(1340, 160), isConsumable = true),
            createEnvironmentObject(BLOCK, Point(2000, 180), isConsumable = true),
            createEnvironmentObject(BLOCK, Point(2600, 160), isConsumable = true),
            createEnvironmentObject(BLOCK, Point(2650, 180), isConsumable = false),
            createEnvironmentObject(BLOCK, Point(3500, 160), isConsumable = false),
            createEnvironmentObject(BLOCK, Point(3550, 140), isConsumable = true),
            createEnvironmentObject(BLOCK, Point(4000, 170), isConsumable = false),
            createEnvironmentObject(BLOCK, Point(4200, 200), isConsumable = true),
            createEnvironmentObject(BLOCK, Point(4300, 210), isConsumable = true),
    
            createEnvironmentObject(HIDDEN_BLOCK, Point(1170, 180), isConsumable = true),
            createEnvironmentObject(HIDDEN_BLOCK, Point(1140, 170), isConsumable = true),
            createEnvironmentObject(HIDDEN_BLOCK, Point(1110, 160), isConsumable = true),
            createEnvironmentObject(HIDDEN_BLOCK, Point(1080, 150), isConsumable = true)
        )
        
        this.specialObjects.append(
            createSpecialObject(COIN, Point(402, 145)),
            createSpecialObject(COIN, Point(1202, 140)),
            createSpecialObject(COIN, Point(1272, 95)),
            createSpecialObject(COIN, Point(1342, 40)),
            createSpecialObject(COIN, Point(1650, 145)),
            createSpecialObject(COIN, Point(2650, 145)),
            createSpecialObject(COIN, Point(3000, 135)),
            createSpecialObject(COIN, Point(3400, 125)),
            createSpecialObject(COIN, Point(4200, 145)),
            createSpecialObject(COIN, Point(4600, 40))
        )
        
        this.characters.append(this.player,
            createEnemyCharacter(GOOMBA, Point(800, GOOMBA.defaultOffsetY), canJump = true),
            createEnemyCharacter(KOOPA, Point(950, KOOPA.defaultOffsetY), canJump = false),
            createEnemyCharacter(KOOPA, Point(1050, KOOPA.defaultOffsetY), canJump = true)
        )
    }
    
    override def environmentObjectsList: List[GameObject] = this.objects.toList
    
    override def charactersList: List[GameCharacter] = this.characters.toList
    
    override def specialObjectsList: List[SpecialObject] = this.specialObjects.toList
    
    override def mainCharacter: MainCharacter = this.player
}