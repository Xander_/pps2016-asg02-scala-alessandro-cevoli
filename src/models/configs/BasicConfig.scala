package models.configs

/**
  * Created by Alessandro on 09/04/2017.
  */
trait BasicConfig {
    def name:String
    
    def width:Int
    
    def height:Int
    
    def frequency:Int
}
