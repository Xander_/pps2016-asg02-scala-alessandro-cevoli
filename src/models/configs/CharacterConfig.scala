package models.configs

import java.awt.Image

import controllers.characters.CharacterController
import utils.AnimationLogics._
import views.commons.AnimatedElementViewer

import scala.language.postfixOps

/**
  * Created by Alessandro on 08/04/2017.
  */
trait CharacterConfig extends BasicConfig{
    
    def apply(view:AnimatedElementViewer[CharacterController]) : Image
    
    def deathOffsetY(character: CharacterController):Int
    
    def defaultOffsetY:Int
    
    def canJump:Boolean
    
    override def toString: String = "It's me! " + this.name + "!"
    
    override def equals(obj: scala.Any): Boolean = obj match {
            case that: CharacterConfig =>
                this.name == that.name && this.width == that.width && this.height == that.height
            case _ => false
        }
}

object CharacterConfig extends Enumeration{
    
    val PAUSE = 30
    val JUMPING_LIMIT = 50
    
    private[this] case class CharacterConfigImpl(id:Int, name:String, canJump:Boolean,
                                                 private val behaviour: AnimatedElementViewer[CharacterController] => Image,
                                                 frequency:Int, width:Int, height:Int,
                                                 deathOffsetY:Int, defaultOffsetY:Int) extends CharacterConfig {
        
        override def apply(view:AnimatedElementViewer[CharacterController]):Image = behaviour(view)
        override def deathOffsetY(character: CharacterController) :Int = if (character.isAlive) 0 else deathOffsetY
    }
    
    val MARIO:CharacterConfig = CharacterConfigImpl(0, Res.MARIO, canJump = true,
        view => if (view.getController.isAlive) if (view.getController.isJumping)
            jumpAnimation(view) else walkAnimation(view) else deathAnimation(view),
        5, 28, 50, 0, 243)
    
    val KOOPA:CharacterConfig = CharacterConfigImpl(1, Res.KOOPA, canJump = true,
        view => if (view.getController.isAlive) if (view.getController.isJumping)
            jumpAnimation(view) else walkAnimation(view) else deathAnimation(view),
        20, 43, 50, 30, 243)
    
    val GOOMBA:CharacterConfig = CharacterConfigImpl(2, Res.GOOMBA, canJump = true,
        view => if (view.getController.isAlive) if (view.getController.isJumping)
            jumpAnimation(view) else walkAnimation(view) else deathAnimation(view),
        20, 27, 30, 20, 263)
}