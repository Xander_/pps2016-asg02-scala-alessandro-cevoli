package models.configs

import java.awt.Image

import controllers.objects.ObjectController
import utils.Utils
import views.commons.{AnimatedElementViewer, SimpleElementViewer}

/**
  * Created by Alessandro on 08/04/2017.
  */
trait ObjectConfig extends BasicConfig{
    
    def apply(view: SimpleElementViewer[ObjectController]): Image
    
    def isSpecial: Boolean
    
    def canBeTotallyConsumed: Boolean
    
    override def toString: String = "I'm a " + name
    
    override def equals(obj: scala.Any): Boolean = obj match {
        case that : ObjectConfig =>
            this.name == that.name && this.height == that.height && this.width == that.width
        case _ => false
    }
}

object ObjectConfig extends Enumeration{
    
    private[this] case class ObjectConfigImpl(name:String, behaviour: SimpleElementViewer[ObjectController] => Image,
                                              width:Int, height:Int, frequency:Int,
                                              isSpecial:Boolean, canBeTotallyConsumed:Boolean) extends ObjectConfig {
        
        override def apply(view: SimpleElementViewer[ObjectController]): Image = behaviour(view)
    }
    
    val PIPE:ObjectConfig = ObjectConfigImpl(Res.PIPE, view => Utils.getImage(Res.IMG_PIPE),
        43, 65, 0, isSpecial = false, canBeTotallyConsumed = false)
    
    val BLOCK:ObjectConfig = ObjectConfigImpl(Res.BLOCK, view => Utils.getImage(Res.IMG_BLOCK),
        30, 30, 0, isSpecial = false, canBeTotallyConsumed = true)
    
    val HIDDEN_BLOCK:ObjectConfig = ObjectConfigImpl(Res.EMPTY_BLOCK,
        view => view.getController match {
            case c : ObjectController =>
                if (!c.isConsumed) Utils.getImage(Res.IMG_EMPTY_BLOCK)
                else Utils.getImage(Res.IMG_GOLD_BLOCK)
            case _ => null
        }, 30, 30, 0, isSpecial = false, canBeTotallyConsumed = false)
    
    val COIN:ObjectConfig = ObjectConfigImpl(Res.COIN, {
            case v: AnimatedElementViewer[ObjectController] =>
                if (v.getAnimationStepCounter % v.getController.getConfig.frequency == 0)
                    Utils.getImage(Res.IMG_COIN1) else Utils.getImage(Res.IMG_COIN2)
            case _ => Utils.getImage(Res.IMG_COIN1)
        }, 30, 30, 20, isSpecial = true, canBeTotallyConsumed = true)
}
