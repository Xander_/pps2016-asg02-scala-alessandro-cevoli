package models.configs

/**
  * Created by Alessandro on 08/04/2017.
  */
trait AudioConfig {
    def soundSource:String
    
    override def toString: String = "Audio track is: " + soundSource
    
    override def equals(obj: scala.Any): Boolean = obj match {
        case that : AudioConfig => this.soundSource == that.soundSource
        case _ => false
    }
}

object AudioConfig extends Enumeration{
    private[this] case class AudioConfigImpl(soundSource:String) extends AudioConfig
    
    val AUDIO_MARIO:AudioConfig = AudioConfigImpl(Res.AUDIO_MARIO)
    val AUDIO_JUMP:AudioConfig = AudioConfigImpl(Res.AUDIO_JUMP)
    val AUDIO_COIN:AudioConfig = AudioConfigImpl(Res.AUDIO_COIN)
}
