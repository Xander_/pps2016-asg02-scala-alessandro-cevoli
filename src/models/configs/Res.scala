package models.configs

import utils.Utils
import java.awt._

/**
  * @author Roberto Casadei
  */
object Res { //public static final String sep = "/";
    private val SPRITES_ROOT = "/sprites/"
    private val AUDIO_ROOT = "/audio/"
    private val SPRITE_EXTENTION = ".png"
    //public static final String IMGP_STATUS_NORMAL = "";
    private val STATUS_ACTIVE = "Walking"
    private val STATUS_DEAD = "Dead"
    private val STATUS_IDLE = "Idle"
    private val STATUS_JUMP = "Jumping"
    
    val DIR_SX = "Left"
    val DIR_DX = "Right"
    val GOOMBA = "Goomba"
    val KOOPA = "Koopa"
    val MARIO = "Mario"
    val BLOCK = "Block"
    val EMPTY_BLOCK = "Empty" + BLOCK
    val GOLD_BLOCK = "Gold" + BLOCK
    val COIN = "Coin"
    val MONEY1: String = COIN + "1"
    val MONEY2: String = COIN + "2"
    val PIPE = "Pipe"
    val IMG_BLOCK: String = SPRITES_ROOT + BLOCK + SPRITE_EXTENTION
    val IMG_EMPTY_BLOCK: String = SPRITES_ROOT + EMPTY_BLOCK + SPRITE_EXTENTION
    val IMG_GOLD_BLOCK: String = SPRITES_ROOT + GOLD_BLOCK + SPRITE_EXTENTION
    val IMG_COIN1: String = SPRITES_ROOT + MONEY1 + SPRITE_EXTENTION
    val IMG_COIN2: String = SPRITES_ROOT + MONEY2 + SPRITE_EXTENTION
    val IMG_PIPE: String = SPRITES_ROOT + PIPE + SPRITE_EXTENTION
    val IMG_BACKGROUND: String = SPRITES_ROOT + "Background" + SPRITE_EXTENTION
    val IMG_CASTLE_START: String = SPRITES_ROOT + "StartingCastle" + SPRITE_EXTENTION
    val START_ROADSIGN: String = SPRITES_ROOT + "StartRoadSign" + SPRITE_EXTENTION
    val IMG_CASTLE_FINAL: String = SPRITES_ROOT + "EndingCastle" + SPRITE_EXTENTION
    val IMG_FLAGPOLE: String = SPRITES_ROOT + "FlagPole" + SPRITE_EXTENTION
    val AUDIO_COIN: String = AUDIO_ROOT + "coin.wav"
    val AUDIO_MARIO: String = AUDIO_ROOT + "mario.wav"
    val AUDIO_JUMP: String = AUDIO_ROOT + "jump.wav"
    
    def buildIdleImage(charName: String, direction: String): Image = Utils.getImage(SPRITES_ROOT + charName + STATUS_IDLE + direction + SPRITE_EXTENTION)
    
    def buildWalkImage(charName: String, direction: String): Image = Utils.getImage(SPRITES_ROOT + charName + STATUS_ACTIVE + direction + SPRITE_EXTENTION)
    
    def buildJumpImage(charName: String, direction: String): Image = Utils.getImage(SPRITES_ROOT + charName + STATUS_JUMP + direction + SPRITE_EXTENTION)
    
    def buildDeathImage(charName: String, direction: String): Image = Utils.getImage(SPRITES_ROOT + charName + STATUS_DEAD + direction + SPRITE_EXTENTION)
}