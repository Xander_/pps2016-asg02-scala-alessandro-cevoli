package models.configs

import java.awt.Image

import models.commons.Point
import utils.Utils

/**
  * Created by Alessandro on 08/04/2017.
  */
trait GameEnvironmentConfig {
    def image:Image
    def position:Point[Integer]
    
    override def toString: String = image.toString
    
    override def equals(obj: scala.Any): Boolean = obj match {
        case that : GameEnvironmentConfig => this.image == that.image && this.position == that.position
        case _ => false
    }
}

object GameEnvironmentConfig extends Enumeration{
    
    var MOV: Int = 0
    var X_POS: Int = -1
    val FLOOR_DEFAULT_OFFSET_Y: Int = 293
    val HEIGHT_LIMIT: Int = 0
    var ENVIRONMENT_MIN_X: Int = 0
    var ENVIRONMENT_MAX_X: Int = 4600
    val BACKGROUND_A_DEFAULT_X_POS: Int = -50
    val BACKGROUND_B_DEFAULT_X_POS: Int = 750
    val BACKGROUND_FLIPPING_OFFSET: Int = 800
    
    private[this] case class GameEnvironmentConfigImpl(image: Image, position:Point[Integer]) extends GameEnvironmentConfig
    
    val BACKGROUND_A:GameEnvironmentConfig = GameEnvironmentConfigImpl(Utils.getImage(Res.IMG_BACKGROUND), Point(-50,0))
    val BACKGROUND_B:GameEnvironmentConfig = GameEnvironmentConfigImpl(Utils.getImage(Res.IMG_BACKGROUND), Point(750,0))
    val STARTING_CASTLE:GameEnvironmentConfig = GameEnvironmentConfigImpl(Utils.getImage(Res.IMG_CASTLE_START), Point(10,95))
    val ENDING_CASTLE:GameEnvironmentConfig = GameEnvironmentConfigImpl(Utils.getImage(Res.IMG_CASTLE_FINAL), Point(4850, 145))
    val STARTING_SIGN:GameEnvironmentConfig = GameEnvironmentConfigImpl(Utils.getImage(Res.START_ROADSIGN), Point(220,234))
    val ENDING_FLAGPOLE:GameEnvironmentConfig = GameEnvironmentConfigImpl(Utils.getImage(Res.IMG_FLAGPOLE), Point(4650, 115))
}