package tests;

import controllers.characters.CharacterController;
import models.commons.Point$;
import models.configs.Res;
import models.factories.*;
import org.junit.Before;
import org.junit.Test;
import views.commons.AnimatedElementViewer;

import static models.configs.CharacterConfig$.MODULE$;
import static org.junit.Assert.assertTrue;

/**
 *
 * Created by Xander_C on 14/03/2017.
 */
public class AnimatedElementViewerTest {

    private AnimatedElementViewer<CharacterController> aChar;
    private AnimatedElementViewer<CharacterController> enemy;

    @Before
    public void init(){

        aChar = GameElementsFactory$.MODULE$.createPlayerCharacter(MODULE$.MARIO(), Point$.MODULE$.apply(0, 0)).getViewer();

        enemy = GameElementsFactory$.MODULE$.createEnemyCharacter(MODULE$.GOOMBA(), Point$.MODULE$.apply(1,0), false).getViewer();
    }

    @Test
    public void assertCharacterAliveAfterCreation(){

        assertTrue(aChar.getController().isAlive());
    }

    @Test
    public void assertCharacterIdleAfterCreation(){

        assertTrue(aChar.getController().getConfig().apply(aChar).equals(Res.buildIdleImage(Res.MARIO(), Res.DIR_DX())));
        aChar.getController().setToRight(false);
        assertTrue(aChar.getController().getConfig().apply(aChar).equals(Res.buildIdleImage(Res.MARIO(), Res.DIR_SX())));

    }

    @Test
    public void assertMovingAnimationCharacter() {

        aChar.getController().setMoving(true);
        aChar.getController().setToRight(true);
        aChar.setAnimationStepCounter(aChar.getController().getConfig().frequency()-1);

        assertTrue(aChar.getController().getConfig().apply(aChar).equals(Res.buildWalkImage(Res.MARIO(), Res.DIR_DX())));

        aChar.getController().setToRight(false);
        assertTrue(aChar.getController().getConfig().apply(aChar).equals(Res.buildWalkImage(Res.MARIO(), Res.DIR_SX())));
    }

    @Test
    public void assertJumpingAnimationMain(){

        aChar.getController().setJumping(true);

        assertTrue(aChar.getController().getConfig().apply(aChar).equals(Res.buildJumpImage(Res.MARIO(), Res.DIR_DX())));

        aChar.getController().setToRight(false);

        assertTrue(aChar.getController().getConfig().apply(aChar).equals(Res.buildJumpImage(Res.MARIO(), Res.DIR_SX())));
    }

    @Test
    public void assertDeathAnimationCharacter() {

        aChar.getController().setAlive(false);

        assertTrue(aChar.getController().getConfig().apply(aChar).equals(Res.buildDeathImage(Res.MARIO(), Res.DIR_DX())));

        aChar.getController().setToRight(false);

        assertTrue(aChar.getController().getConfig().apply(aChar).equals(Res.buildDeathImage(Res.MARIO(), Res.DIR_SX())));
    }

    @Test
    public void assertCollision(){

        assertTrue(aChar.getController().computeCollision(enemy.getController()));
    }

    @Test
    public void assertMainDeadAfterBackOrAheadCollision(){

        aChar.getController().computeCollision(enemy.getController());

        assertTrue(!aChar.getController().isAlive());
        assertTrue(enemy.getController().isAlive());
    }

    @Test
    public void assertMainCharacterAliveAfterBottomCollision(){

        aChar.getController().setY(aChar.getController().getHeight());
        aChar.getController().computeCollision(enemy.getController());

        assertTrue(aChar.getController().isAlive());
    }
}